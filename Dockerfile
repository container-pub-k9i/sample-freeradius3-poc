# FreeRADIUS
# - https://hub.docker.com/r/freeradius/freeradius-server/
# - https://github.com/FreeRADIUS/freeradius-server/blob/v3.0.x/scripts/docker/ubuntu18/Dockerfile

# for Docker Container
# - https://github.com/2stacks/docker-freeradius/blob/b9dab81ad9017fc416f9cffe414f9a36a61091f2/docs/README.md

FROM freeradius/freeradius-server:3.0.21

#
# NOTE: ENV affects only in container or RUN command (not in Dockerfile)
#
ENV IS_DOCKER=1

# ENV DEBIAN_FRONTEND=noninteractive is discouraged in Docker FAQ
# see: https://docs.docker.com/engine/faq/#why-is-debian_frontendnoninteractive-discouraged-in-dockerfiles
#
# DEBIAN_FRONTEND supperss dpkg-reconfigure interactive input.
ENV DEBIAN_FRONTEND=noninteractive
RUN set -eux; \
    apt-get update; \
    apt-get install -y tzdata; \
    ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime; \
    dpkg-reconfigure tzdata;

# install ubuntu basic tools
RUN set -eux; \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    tree \
    git \
    jq \
    less colordiff \
    curl wget netcat iputils-ping net-tools iproute2

# install other
RUN set -eux; \
    apt-get install -y --no-install-recommends \
    vim-tiny \
    screen tmux
COPY files/screenrc /etc/freeradius/.screenrc

# ----------------------------------------------------------------------
RUN chsh -s /bin/bash
WORKDIR /etc/raddb

COPY raddb/* /etc/raddb/

RUN mv -v /etc/raddb/sites-enabled /etc/raddb/sites-enabled.org && mkdir /etc/raddb/sites-enabled
COPY raddb/sites-enabled/* /etc/raddb/sites-enabled/

COPY files/customize_mods-enabled.sh /usr/local/bin/
RUN mv -v mods-enabled mods-enabled.org && mkdir /etc/freeradius/mods-enabled
RUN /usr/local/bin/customize_mods-enabled.sh

# ----------------------------------------------------------------------
USER 101
ENV HOME /etc/freeradius

# ----------------------------------------------------------------------
# for FreeRADIUS
ENV RADIUS_KEY=testing123
ENV RAD_CLIENTS=100.0.0.0/8
ENV RAD_AUTH_DEBUG=no

EXPOSE 1812/udp 1813/udp
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["radiusd", "-X"]
