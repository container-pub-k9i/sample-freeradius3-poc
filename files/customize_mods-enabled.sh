#!/bin/bash
set -euo pipefail
cd /etc/freeradius/mods-enabled

USE_MODULES='
  always
  attr_filter
  chap
  date
  detail
  detaillog
  echo  
  exec
  expr
  files
  linelog
  logintime
  pap
  preprocess
  realm
  replicate
  unix
  unpack
'

for m in ${USE_MODULES}; do
  ln -sv ../mods-available/${m} .
done

